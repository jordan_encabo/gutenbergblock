<div class="textwithimage">
    <div class="textwithimage-container textwithimage-container--flex">
        <div class="textwithimage-column textwithimage-column--50 textwithimage-column--text">
            <img class="textwithimage-icon--top" src ="<?= block_field( 'icon' ); ?>" />
            <h2 class="textwithimage-title"><?= block_field( 'title' ); ?></h2>
            <p class="textwithimage-description"><?= block_field('description') ?></p>
            <a class="textwithimage-button textwithimage-button--link" href="<?= block_field( 'cta-link' ); ?>">
                <img class="textwithimage-button--icon" src ="<?= block_field( 'cta-icon' ); ?>" />
                <span class="textwithimage-button--label"><?= block_field( 'cta-label' ); ?></span>
            </a>
        </div>
        <div class="textwithimage-column textwithimage-column--50 textwithimage-column--image">
            <img src="<?= block_field( 'image' ); ?>" />
        </div>
    </div>
</div>